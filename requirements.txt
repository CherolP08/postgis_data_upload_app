flake8==5.0.4
black==22.10.0
psycopg2-binary==2.9.5
python-dotenv==0.21.0
openpyxl==3.0.10
pandas==1.5.1
pytest==7.2.0