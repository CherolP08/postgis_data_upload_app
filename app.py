import os
import sys
from dotenv import load_dotenv
import pandas as pd
import psycopg2

# Load config file
load_dotenv()

# connection uri
conn_params = {
    "host": os.getenv('HOST'),
    "port": os.getenv('PORT'),
    "database": os.getenv('DB_NAME'),
    "user": os.getenv('DB_USER'),
    "password": os.getenv('PWD')
}


def connect(conn_params):
    """Connect to the Postgres database"""
    conn = None
    try:
        print('Connecting to the database...')
        conn = psycopg2.connect(**conn_params)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit(1)
    print("Connection successful")
    return conn


def create(conn, sql_file):
    """ Execute query to create the data schema and table"""
    ret = 0
    cursor = conn.cursor()
    try:
        cursor.execute(open(sql_file, 'r').read())
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    cursor.close()
    print('Create Query Complete!')
    return ret


def insert(conn, data, schema_name, table_name):
    """Insert data into PostGIS"""
    cursor = conn.cursor()
    try:
        for i in data.index:
            fields = [data.at[i, col] for col in list(data.columns)]
            query = """INSERT INTO {}.{}(park_name, province, physical_address, geom)
            VALUES ('{}', '{}', '{}', ST_GeomFromText('POINT({} {})', 4326));
            """.format(schema_name, table_name, fields[0], fields[1], fields[2], fields[3], fields[4])
            cursor.execute(query)
            conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print('Insert Query Complete!')


def main():
    # Create database connection
    conn = connect(conn_params)
    # Create a new schema and table
    create(conn, 'schema.sql')
    # Read the excel file
    excel_data = pd.read_excel(os.getenv('DATA_FILE'))
    data = pd.DataFrame(excel_data, columns=['Name', 'State', 'Address', 'Longitude', 'Latitude'])
    # Load the data into a table
    insert(conn, data,  os.getenv('SCHEMA_NAME'), os.getenv('TABLE_NAME'))
    # Close the connection
    conn.close()


if __name__ == "__main__":
    main()
