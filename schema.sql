DROP TABLE IF EXISTS national_parks_schema.national_parks;
DROP SCHEMA IF EXISTS national_parks_schema;
-- CREATE EXTENSION postgis;
CREATE SCHEMA national_parks_schema;
CREATE TABLE national_parks_schema.national_parks (
    id SERIAL PRIMARY KEY,
    park_name TEXT NOT NULL,
    province TEXT NOT NULL,
    physical_address TEXT NOT NULL,
    geom geometry(POINT, 4326)
);

SET search_path to national_parks_schema, public;
